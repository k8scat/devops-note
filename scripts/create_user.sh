#!/bin/bash

if [ -z "$1" ];then
    echo "please input user name"
    exit 1
fi

if [ ! -f "${1}.pubkey" ];then
    echo "public key ${1}.pubkey not found"
    exit 1
fi

pubkey=`cat ${1}.pubkey`

useradd -m -s /bin/bash $1
cd /home/$1
ssh_root=/home/$1/.ssh
mkdir $ssh_root
chmod 700 $ssh_root
cd $ssh_root
touch authorized_keys
chmod 600 authorized_keys
echo $pubkey >> authorized_keys

chown -R $1:$1 $ssh_root
