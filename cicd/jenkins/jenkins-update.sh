#!/bin/bash

docker stop jenkins
docker rm jenkins
docker pull jenkins/jenkins:latest

docker run -d --name jenkins \
-v /srv/jenkins:/var/jenkins_home \
-v /etc/localtime:/etc/localtime \
-v /etc/timezone:/etc/timezone \
-p 50000:50000 \
-p 8080:8080 \
jenkins/jenkins:latest