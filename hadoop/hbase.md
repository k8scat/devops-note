# HBase

Apache HBase™ is the Hadoop database, a distributed, scalable, big data store.

```bash
# 下载
wget https://mirrors.tuna.tsinghua.edu.cn/apache/hbase/stable/hbase-1.4.11-bin.tar.gz
tar -xvf hbase-1.4.11-bin.tar.gz
rm -f hbase-1.4.11-bin.tar.gz
mv hbase-1.4.11-bin.tar.gz hbase

# JAVA_HOME
vi conf/hbase-env.sh
# The java implementation to use.  Java 1.7+ required.
export JAVA_HOME=/usr/lib/jvm/java

# 配置HBase
vi conf/hbase-site.sh
<property>
    <name>hbase.rootdir</name>
    <value>file:///home/vagrant/hbase</value>
</property>
<property>
    <name>hbase.zookeeper.property.dataDir</name>
    <value>/home/vagrant/zookeeper</value>
</property>
<property>
    <name>hbase.unsafe.stream.capability.enforce</name>
    <value>false</value>
</property>

# 添加环境变量
sudo vi /etc/profile
# 添加以下内容


```