# Hadoop

## Todo

* [ ] ssh to machine
* [ ] use ansible

9000
9870 mr
8088 yarn

## Setup

### Base Environment

Vagrant + Centos7(2G)

> Cluster

* master  192.168.33.11
* worker1   192.168.33.12
* worker2   192.168.33.13

```

# vagrant用户的密码: 123456
sudo passwd vagrant

# 安装jdk
sudo yum install -y java-1.8.0-openjdk-devel.x86_64

# 配置JAVA_HOME: vi etc/hadoop/hadoop-env.sh, 必须配置
# master: ERROR: JAVA_HOME is not set and could not be found.
export JAVA_HOME=/usr/lib/jvm/java

# /etc/profile
export HADOOP_HOME=/home/vagrant/hadoop
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
# export JAVA_HOME=/usr/lib/jvm/java

```

### Setup passphraseless ssh

```bash
$ ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
$ chmod 0600 ~/.ssh/authorized_keys

```

### Some files

* Read-only default configuration - `core-default.xml`, `hdfs-default.xml`, `yarn-default.xml` and `mapred-default.xml`.
* Site-specific configuration - `etc/hadoop/core-site.xml`, `etc/hadoop/hdfs-site.xml`, `etc/hadoop/yarn-site.xml` and `etc/hadoop/mapred-site.xml`.


### Hadoop startup

```bash
hdfs namenode -format node1 \
hdfs --daemon start namenode \
hdfs --daemon start datanode \
start-dfs.sh \

yarn --daemon start resourcemanager \
yarn --daemon start nodemanager \
yarn --daemon start proxyserver \
start-yarn.sh \

mapred --daemon start historyserver

```

### Set selinux disabled

```bash
vi /etc/selinux/config

# SELINUX=disabled

```

### Permission denied (publickey,gssapi-keyex,gssapi-with-mic).

### 修改主机名

```bash
vi /etc/hostname
# master

sudo reboot

hostname
> master

```

### 建立主机名和ip的映射

```bash
vi /etc/hosts

192.168.33.11   master
192.168.33.12   slave1
192.168.33.13   slave2

```

### the 3.x version hadoop have changed the slave file to '$HADOOP_HOME/etc/workers' file

## Refer

* [Setting up a Single Node Cluster](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html)
* [Hadoop 新 MapReduce 框架 Yarn 详解](https://www.ibm.com/developerworks/cn/opensource/os-cn-hadoop-yarn/)