# GitLab Runner

## Documents

+ [Run GitLab Runner in a container](https://docs.gitlab.com/runner/install/docker.html)

+ [Docker section of Registering Runners](https://docs.gitlab.com/runner/register/index.html#docker)

+ [One-line registration command](https://docs.gitlab.com/runner/register/#one-line-registration-command)