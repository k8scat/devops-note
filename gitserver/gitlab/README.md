# GitLab

https://docs.gitlab.com/omnibus/docker/

GitLab Tags: https://hub.docker.com/r/gitlab/gitlab-ce/tags

不要轻易更新版本

## `root` is the first administrator.

## TODOs

+ [ ] Pages
+ [ ] Cluster

## Cluster

## Run

~~1 Core + 2GB RAM (+ 4GB Swap)~~

2c4g

## References

+ [Requirements](https://docs.gitlab.com/ee/install/requirements.html)
+ [How do I configure swappiness](https://askubuntu.com/questions/103915/how-do-i-configure-swappiness/103916#103916)
+ [Continuous Integration and Deployment Admin settings](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html)
+ [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
+ [GitLab Pages administration](https://docs.gitlab.com/ee/administration/pages/index.html)
+ [Administrator documentation](https://docs.gitlab.com/ee/administration/)
+ [Scaling and High Availability](https://docs.gitlab.com/ee/administration/high_availability/README.html#high-availability-architecture-examples)
+ [Configuring Gitaly for Scaled and High Availability](https://docs.gitlab.com/ee/administration/high_availability/gitaly.html#configuring-gitaly-for-scaled-and-high-availability)
+ [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
