#!/bin/bash

docker stop gitlab
docker rm gitlab
docker pull gitlab/gitlab-ce:latest

docker run --detach \
--hostname git.ncucoder.com \
--publish 2443:443 --publish 22:22 \
--name gitlab \
--restart always \
--volume /srv/gitlab/config:/etc/gitlab \
--volume /srv/gitlab/logs:/var/log/gitlab \
--volume /srv/gitlab/data:/var/opt/gitlab \
--volume /etc/localtime:/etc/localtime \
--volume /etc/timezone:/etc/timezone \
gitlab/gitlab-ce:latest