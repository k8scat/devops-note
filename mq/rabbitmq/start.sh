#!/bin/bash

# docker run -d --name rabbitmq --hostname my-rabbit rabbitmq:3

rabbitmq_container=first-rabbit

docker stop ${rabbitmq_container}
docker rm ${rabbitmq_container}
docker run -d --name ${rabbitmq_container} --hostname first-rabbit -p 5672:5672 -p 8080:15672 rabbitmq:3-management