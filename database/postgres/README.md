# PostgreSQL

https://www.postgresql.org/

https://hub.docker.com/_/postgres

## Docker

```bash
docker run --name postgres -e POSTGRES_PASSWORD=123456 -d postgres

```