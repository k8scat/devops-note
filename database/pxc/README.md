# MySQL 集群搭建

## PXC 与 Replication

+ PXC 同步复制，强一致性

docker image: https://hub.docker.com/r/percona/percona-xtradb-cluster

+ Replication 异步复制，弱一致性

## 负载均衡

+ Haproxy

docker image: https://hub.docker.com/_/haproxy

+ Keepalived

http://www.keepalived.org/index.html

+ ProxySQL

docker image: https://hub.docker.com/r/percona/proxysql

## 热备

+ xtrabackup

## MySQL 衍生版本

+ Percona(XtraDB存储引擎)
+ MariaDB
+ Drizzle
+ TokuDB