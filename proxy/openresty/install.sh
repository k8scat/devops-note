#!/bin/bash

# LSB Version:    :core-4.1-amd64:core-4.1-noarch
# Distributor ID: CentOS
# Description:    CentOS Linux release 7.8.2003 (Core)
# Release:        7.8.2003
# Codename:       Core

set -e

saveDir=/data
if [ ! -d $saveDir ];then
    mkdir $saveDir
fi
cd $saveDir

# openresty
yum install pcre-devel openssl-devel gcc curl perl
wget https://openresty.org/download/openresty-1.17.8.2.tar.gz
tar -zxvf openresty-1.17.8.2.tar.gz
cd openresty-1.17.8.2
./configure -j2 --with-http_v2_module
gmake -j2
gmake install
cat >> /etc/profile << EOF
export OPENRESTY_DIR=/usr/local/openresty
export PATH=\$PATH:\$OPENRESTY_DIR/nginx/sbin:\$OPENRESTY_DIR/luajit/bin
EOF
cd -

# luarocks
wget https://luarocks.org/releases/luarocks-3.3.1.tar.gz
tar -zxf luarocks-3.3.1.tar.gz
cd luarocks-3.3.1
./configure --prefix=/usr/local/openresty/luajit \
--with-lua=/usr/local/openresty/luajit \
--lua-suffix=jit \
--with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
cd -

# clean
rm -f openresty-1.17.8.2.tar.gz luarocks-3.3.1.tar.gz