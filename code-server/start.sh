#!/bin/bash

docker stop vscode
docker rm vscode
docker pull codercom/code-server:latest

docker run -t -d \
--name vscode \
-p 8443:8443 \
-v /srv/vscode:/root/project \
codercom/code-server --allow-http --no-auth