# Docker

## Portainer

https://github.com/portainer/portainer

## Enable docker remote connect

```shell script
systemctl edit docker.service

# Add content below
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H unix:///var/run/docker.sock -H tcp://0.0.0.0:2375

systemctl daemon-reload && systemctl restart docker

```

https://blog.csdn.net/cao0507/article/details/83043485

## Accelerator

```
$ ./accelerator.sh
```

#