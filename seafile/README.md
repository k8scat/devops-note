# seafile

https://manual-cn.seafile.com/deploy/deploy_with_docker.html

## Todos

+ [ ] Cannot upload files which are bigger than 1MB

## Use QQ Mail

```
EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.qq.com'        # smpt 服务器
EMAIL_HOST_USER = 'username@qq.com'    # 用户名和域名
EMAIL_HOST_PASSWORD = 'password'    # 密码
EMAIL_PORT = '465'
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
```