#!/bin/bash

docker run -d --name seafile \
  -e SEAFILE_SERVER_HOSTNAME=file.ncucoder.com \
  -e SEAFILE_ADMIN_EMAIL=admin@ncucoder.com \
  -e SEAFILE_ADMIN_PASSWORD=admin \
  -v /srv/seafile-data:/shared \
  -p 80:80 \
  seafileltd/seafile:latest